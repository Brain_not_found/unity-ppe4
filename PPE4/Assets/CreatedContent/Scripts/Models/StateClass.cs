﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateClass : MonoBehaviour
{
    public string Name { get; set; }
    public int DamageRate { get; set; }

    public StateClass() { }
}
